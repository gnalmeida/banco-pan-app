# Introdução 
O Banco Pan App é o serviço de Empréstimo Pessoal que se autentica no Banco Pan Login(SSO), feito para o chalenge final do MBA em engenharia de Software da FIAP.

# Tecnologias utilizadas:
- React Native
- react-native-app-auth

## Comandos para executar o projeto:
- npm install
- npm run android
