import React, { useState, useCallback, useMemo } from 'react';
import { UIManager, Alert } from 'react-native';
import { authorize, refresh, revoke, prefetchConfiguration } from 'react-native-app-auth';
import { Page, Button, ButtonContainer, Form, FormLabel, FormValue, Heading } from './components';
import jwt_decode from "jwt-decode";

const ISSUER = 'https://bancopanlogin.azurewebsites.net';

const configs = {
  identityserver: {
    issuer: ISSUER,
    clientId: 'react_native',
    redirectUrl: 'bancopan.app:/oauthredirect',
    additionalParameters: {},
    scopes: ['openid', 'profile'],
    usePKCE: true,

    serviceConfiguration: {
      authorizationEndpoint: `${ISSUER}/connect/authorize`,
      tokenEndpoint: `${ISSUER}/connect/token`,
      revocationEndpoint: `${ISSUER}/connect/revoke`
    }
  }
};

const defaultAuthState = {
  hasLoggedInOnce: false,
  provider: '',
  accessToken: '',
  accessTokenExpirationDate: '',
  refreshToken: '',
  authorizeAdditionalParameters: ''
};

const App = () => {
  const [authState, setAuthState] = useState(defaultAuthState);
  React.useEffect(() => {
    prefetchConfiguration({
      warmAndPrefetchChrome: true,
      connectionTimeoutSeconds: 5,
      ...configs.identityserver,
    });
  }, []);

  const handleAuthorize = useCallback(
    async provider => {
      try {
        const config = configs[provider];
        const newAuthState = await authorize({
          ...config,
          connectionTimeoutSeconds: 5,
        });

        setAuthState({
          hasLoggedInOnce: true,
          provider: provider,
          ...newAuthState
        });
      } catch (error) {
        Alert.alert('Failed to log in', error.message);
      }
    },
    [authState]
  );

  return (
    <Page>
      {!!authState.accessToken ? (
        // <Form>
        //   <FormLabel>accessToken</FormLabel>
        //   <FormValue>{authState.accessToken}</FormValue>
        //   <FormLabel>accessTokenExpirationDate</FormLabel>
        //   <FormValue>{authState.accessTokenExpirationDate}</FormValue>
        //   <FormLabel>refreshToken</FormLabel>
        //   <FormValue>{authState.refreshToken}</FormValue>
        //   <FormLabel>scopes</FormLabel>
        //   <FormValue>{authState.scopes.join(', ')}</FormValue>
        // </Form>
        <Form>
          <FormValue>Olá, {jwt_decode(authState.accessToken).email}</FormValue>
        </Form>
      ) : (
        <Heading>{authState.hasLoggedInOnce ? 'Goodbye.' : 'Hello, stranger.'}</Heading>
      )}

      <ButtonContainer>
        {!authState.accessToken ? (
          <>
            <Button
              onPress={() => handleAuthorize('identityserver')}
              text="Login"
              color="#039be5"
            />
          </>
        ) : null}
      </ButtonContainer>
    </Page>
  );
}

export default App;

