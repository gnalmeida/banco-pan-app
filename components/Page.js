import React from 'react';
import { ImageBackground, StyleSheet, SafeAreaView, View, Image } from 'react-native';

const Page = ({ children }) => (
    <View style={styles.container}>
      <Image
        style={styles.tinyLogo}
        source={require('../assets/logo_maior.jpg')}      
        />
      <Image
        style={styles.banner}
        source={require('../assets/banner_2.png')}      
        />
      {children}
    </View>
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    paddingTop: 40,
    paddingHorizontal: 10,
    paddingBottom: 10,
  },
  safe: {
    flex: 1,
  },
  tinyLogo: {
    alignSelf: 'center',
    width: 200,
    height: 100,
    resizeMode: 'stretch'
  },
  logo: {
    width: 66,
    height: 58,
  },
  banner: {
    alignSelf: 'center',
    marginTop: 50,
    width: 340,
    height: 200,
    resizeMode: 'stretch'
  },
});

export default Page;
